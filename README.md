# wikt-cli

[![Usable Technology Badge](https://img.shields.io/badge/usable-technology-blue)](https://fkfd.me/ut/landing) This software is a piece of Usable Technology.

## Installation

- From pypi: `pip install wikt-cli`

- From source: clone this repo, `cd` into it, run `python setup.py bdist_wheel`, `pip install dist/wikt_cli-{VERSION}-py3-none-any.whl` with proper permission and/or destination.

## Usage

`wikt <word> [--no-color] [-d|--definition]`

See `wikt -h` for help.

## Example output

The output of `wikt meta --no-color` is:

```
Etymology 1:
    From meta-, back-formed from metaphysics.

    Definitions:
        (adjective) meta (comparative more meta, superlative most meta)
            1. (informal) Self-referential; structured analogously, but at a higher level.

            Examples:
                1. Suppose you have a genie that grants you three wishes. If you wish for infinite wishes, that is a meta wish.
                2. […] in finessing obligations you fail a "meta" kind of obligation.
                3. Besides, I can just hear Vaughan: "Very funny, Stacey, very Charlie Kaufman-esque, very meta, very '97. I can't use it."

Etymology 2:
    From Latin mēta.

    Definitions:
        (noun) meta (plural metas)
            1. Boundary marker.
            2. Either of the conical columns at each end of a Roman circus.


Etymology 3:
    Clipping of metagame.

    Definitions:
        (noun) meta (plural metas)
            1. (video games) Metagame; the most effective tactics and strategies used in a competitive video game.

        (adjective) meta (comparative more meta, superlative most meta)
            1. (video games) Prominent in the metagame; effective and frequently used in competitive gameplay.

            Examples:
                1. I don't think the character will be meta even with the recent buffs.

Etymology 4:
    Clipping of metaoidioplasty.

    Definitions:
        (noun) meta (plural metas)
            1. (informal) Metoidioplasty.
```

## Contributing

1. Using `git.sr.ht`, you don't need an account to contribute! Send "issues" to the mailing list `~fkfd/misc@lists.sr.ht` (remember to use plaintext, not HTML), and "pull/merge requests" as a patch (see [git-send-email](https://git-send-email.io) and [sourcehut mailing list wiki](https://man.sr.ht/lists.sr.ht/)).

2. Can't use a mailing list? A web mirror is available at [this codeberg repo](https://codeberg.org/fakefred/wikt-cli).
